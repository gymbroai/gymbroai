# GymBroAI - [Trophées NSI](https://trophees-nsi.fr/)
> **GymBroAI est un coach sportif utilisant l’intelligence artificielle pour générer des programmes de musculation personnalisés par les utilisateurs prenant en compte le poids, l’âge, la taille, et l’objectif. Il permet d’atteindre des objectifs tels que de la prise de masse musculaire, de la perte de masse graisseuse, du renforcement musculaire et encore plus.**  

# Installation
```
pip install -r requirements.txt
```

# Configuration
Dans le dossier `sources`, créer un fichier `config.json` avec le contenu suivant:
```json
{
  "openai": {
    "secret": "Clé ICI"
  },
  "salt": "CHANGE ME",
  "port": 80,
  "tokens": {
    "auth": "CHANGE ME",
    "authDurationMinutes": 240
  }
}
```
La liste des options avec leurs explications:
| Option | Description |
| ----------- | ----------- |
| `openai.secret` | Clé d'api OpenAI (Voir ci-dessous) |
| `salt` | Le sel utilisé pour hacher les mots de passe |
| `port` | Le port du site (80 permet d'y accéder avec l'url: http://localhost) |
| `tokens.auth` | Le "secret" utiliser pour générer les tokens JWT |
| `tokens.authDurationMinutes` | La durée de validité des tokens |

## Clé api OpenAI:
1. Créer un compte [OpenAI](https://openai.com/)
2. Naviguer a la section [API](https://platform.openai.com/api-keys)
3. Créer une nouvelle Clé avec la permission `ALL`
4. Mettre cette Clé à la place de `Clé ICI` dans la config

## Token JWT / SALT
1. Ouvrir un terminal Python
2. Utiliser le code suivant:
```py
import secrets
print(secrets.token_hex(20))
```
3. Changer dans le fichier de config la valeur de `tokens.auth` par le résultat du programme précédent.