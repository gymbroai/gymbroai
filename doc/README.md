# Root (`/`)
## `index.py`
Ceci est la base du programme, il se base sur tous les fichiers suivants pour son bon fonctionnement.  
Il contient toutes les routes `Flask` permettant de faire les différentes pages Web.

## `config.py`
Permet de récupérer la config du fichier `config.json` et de mettre les données dans une classe pour les rendre faciles d'accès.

## `middlewares.py`
Les middleswares permettent d'appliquer un pré-traitement aux requêtes.
Voici la liste:
- parseUser (Sers à récupérer les informations utilisateur pour savoir s’il est connecté et qui il est)
- requireLogin (Sers à refuser l'accès à une page si l'utilisateur n'es pas connecté, en le redirigeant vers la page d'accueil `/`)

Utilisation:  
Les middlewares s'utilisent comme des décorateurs Python (`@exemple`), il suffit de les mettre avant la fonction de la requête, comme les routes Flask.
```py
import middlewares

...

@app.route("/exemple")
@middlewares.parseUser
def exemple(user=None, prepedUser=None):
  return render_template('exemple.html', user=prepedUser)
```

## `password.py`
Ce fichier permet de hacher les mots de passe.  
Il dispose de 3 fonctions:
- setSalt (permettant de définir le "sel" utilisé pour le hachage des mots de passe)
- hash_password (permettant d'hacher le mot de passe en sha256)
- verify_password (permettant de vérifier la correspondance entre un mot de passe non haché et un mot de passe haché)

## `tokensManager.py`
Ce fichier permet de gérer les tokens d'authentification (Utilisant les JSON Web Tokens).  
Il dispose de 3 fonctions:
- generateAuthToken (permet de générer un token d'authentification pour un utilisateur)
- checkAuthToken (permet de vérifier la validité un token d'authentification)
- parseAuthToken (permet de décoder un token d'authentification)

## `aiManager.py`
Ce fichier permet de générer les exercices en utilisant l'IA.  
Il dispose de 2 fonctions:
- generateWorkout (permet de générer le programme)
- parseWorkout (permet de séparer le programme généré par l'IA)

## `aiPlayground.py`
Ce fichier est utilisé pour tester l'Intelligence Artificielle utilisé pour générer les programmes. Il s'exécute séparément des autres fichiers.



# DB (`/db`)
## `db/manager.py`
Le gestionnaire de la base de données, permettant de faire une instance de la base de données.

## `db/dbInterface.py`
L’interface de la base de données, les différents types de base de données reposent tous là-dessus, permettant de faire des bases de données avec différents logiciels.

## `db/local.py`
Une interface de la basse de donnés en SQLite. D’autres avaient été envisagés mais ont finalement été abandonnés comme MySQL.



# Classes (`/classes`)
## `classes/user.py`
Cette classe utilisateur permets de faciliter la gestion des utilisateurs dans le programme. Elle permet aussi d’essayer la connexion au compte.

## `classes/avatar.py`
Cette classe permets gérer les avatars des utilisateurs. Un système permettant de modifier son avatar était envisagé mais a finalement été abandonné en raison du manque de temps.

## `classes/userCreator.py`
Cette classe permets de facilement créer des utilisateurs dans la base de données.