def ajouter_informations_utilisateur(utilisateur_info):
    poids = float(input("Entrez votre poids en kilogrammes : "))
    utilisateur_info["poids"] = poids

    taille = float(input("Entrez votre taille en mètres : "))
    utilisateur_info["taille"] = taille

    sexe = input("Entrez votre sexe (Homme/Femme) : ").capitalize()
    utilisateur_info["sexe"] = sexe

    age = int(input("Entrez votre âge : "))
    utilisateur_info["age"] = age

utilisateur_info = {}

utilisateur_info["nom"] = input("Entrez votre nom : ")

print("Choisissez le type de physique :")
print("1. Athlétique")
print("2. Massif")
print("3. Sec")
print("4. Tonique")
print("5. Mince")

choix_physique = input("Entrez le numéro correspondant à votre choix : ")

if choix_physique == "1":
    utilisateur_info["physique"] = "Athlétique"
elif choix_physique == "2":
    utilisateur_info["physique"] = "Massif"
elif choix_physique == "3":
    utilisateur_info["physique"] = "Sec"
elif choix_physique == "4":
    utilisateur_info["physique"] = "Tonique"
elif choix_physique == "5":
    utilisateur_info["physique"] = "Mince"
else:
    print("Choix invalide. Veuillez entrer un numéro de 1 à 5.")

ajouter_informations_utilisateur(utilisateur_info)

print("\nInformations de l'utilisateur :")
for cle, valeur in utilisateur_info.items():
    print(f"{cle.capitalize()}: {valeur}")


def calculer_masse_graisseuse(utilisateur_info):
    poids = utilisateur_info["poids"]
    taille = utilisateur_info["taille"]
    sexe = utilisateur_info["sexe"]
    age = utilisateur_info["age"]

    imc = poids / (taille ** 2)

    if sexe == "Homme":
        masse_graisseuse_estimee = 1.2 * imc + 0.23 * age - 10.8 - 5.4
    else:
        masse_graisseuse_estimee = 1.2 * imc + 0.23 * age - 5.4

    return masse_graisseuse_estimee

masse_graisseuse_estimee = calculer_masse_graisseuse(utilisateur_info)

print(f"Estimation de la masse graisseuse : {masse_graisseuse_estimee}%")

def atteindre_objectif_physique(utilisateur_info):
    masse_graisseuse_estimee = calculer_masse_graisseuse(utilisateur_info)

    physique_souhaite = utilisateur_info["physique"]

    seuils_masse_grasse = {
        "Athlétique": 15,
        "Massif": 10,
        "Sec": 8,
        "Tonique": 18,
        "Mince": 25
    }

    seuil_physique_souhaite = seuils_masse_grasse.get(physique_souhaite, None)

    if seuil_physique_souhaite is not None:
        if masse_graisseuse_estimee > seuil_physique_souhaite:
            return "Cut"
        else:
            return "Bulk"
    else:
        return "Type de physique non reconnu"

resultat_objectif = atteindre_objectif_physique(utilisateur_info)

print(f"Objectif physique atteint : {resultat_objectif}")