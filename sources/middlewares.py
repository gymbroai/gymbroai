from functools import wraps
from flask import request, redirect, make_response

from classes.user import User
import tokensManager

def parseUser(function):
  @wraps(function)
  def wrapper(*args, **kwargs):
    authToken = request.cookies.get("authToken")
    
    if(
      authToken == None or
      tokensManager.checkAuthToken(authToken) == False
    ):
      return function(*args, user=None, prepedUser = None, **kwargs)

    user = User(tokensManager.parseAuthToken(authToken)["id"])

    return function(*args, user = user, prepedUser = user.formatForWeb(), **kwargs)
  return wrapper
  
def requireLogin(function):
  @wraps(function)
  def wrapper(*args, **kwargs):
    authToken = request.cookies.get("authToken")
    
    if(
      authToken == None or
      tokensManager.checkAuthToken(authToken) == False
    ):
      res = make_response(redirect("/"))
      res.set_cookie('authToken', value='', expires=0)
      return res

    return function(*args, **kwargs)
  return wrapper