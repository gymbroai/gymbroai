from flask import Flask, render_template, request, make_response, redirect

import middlewares
import password
import tokensManager
import aiManager

from threading import Thread

from db.manager import dbManager
import json

from classes.user import User
from classes.userCreator import UserCreator, UserCreatorSaveError

# Load Config
from config import config

app = Flask(__name__)

# Routes
@app.route("/")
@middlewares.parseUser
def home(user=None, prepedUser=None):
  return render_template('home.html', active="test", user=prepedUser)

# Login
@app.route("/login")
@middlewares.parseUser
def login(user=None, prepedUser=None):
  return render_template('login.html', user=prepedUser)

@app.route("/login", methods = ['POST'])
def login_post():
  username = request.form["username"]
  password = request.form["password"]
   
  user = User.login(username, password)

  if(not user):
    return redirect('/login')

  res = make_response(redirect("/"))
  res.set_cookie("authToken", value=tokensManager.generateAuthToken(user), max_age=config.tokens.authDuration, httponly=True)
  return res

# Register
@app.route("/register")
@middlewares.parseUser
def register(user=None, prepedUser=None):
  return render_template('register.html', user=prepedUser)

@app.route("/register", methods = ['POST'])
def register_post():
  username = request.form["username"]
  fullname = request.form["fullname"]
  email = request.form["email"]
  password = request.form["password"]

  if(
    not request.form
    or not username
    or not fullname
    or not email
    or not password
  ):
    return "Error"
  
  id = UserCreator().setUserName(username).setFullName(fullname).setEmail(email).setPassword(password).save()
  
  if(type(id) == UserCreatorSaveError):
    return "Error"
  
  if(type(id) != int):
    return "Error"
  
  user = User(id)

  res = make_response(redirect("/"))
  res.set_cookie("authToken", value=tokensManager.generateAuthToken(user), max_age=config.tokens.authDuration, httponly=True)
  return res

# Users
def getPlan(user):
  return dbManager.getInstance().db.selectOne("SELECT * FROM programs WHERE USERID=?", (user.userId,))

def getPlanWeeks(user):
  return dbManager.getInstance().db.selectAll("SELECT * FROM programsweeks WHERE USERID=?", (user.userId,))

def prepDayTime(dayTime):
  if(dayTime <= 0): return 0
  if(dayTime > 0 and dayTime < 30): return 30
  if(dayTime > 120): return 120
  return dayTime

def generateWeek(user, weekid):
  plan = getPlan(user)
  if(not plan): return None
  weekProgram = json.dumps(aiManager.parseWorkout(aiManager.generateWorkout(plan[2], plan[3], plan[4], plan[6], weekid, json.loads(plan[7]))))

  dbManager.getInstance().db.execute("UPDATE programsweeks SET CONTENT=? WHERE USERID=? AND WEEKID=?", (weekProgram, user.userId, weekid))
  dbManager.getInstance().db.commit()
  
  return True

@app.route("/plan")
@app.route("/plan/")
@middlewares.parseUser
@middlewares.requireLogin
def plan(user=None, prepedUser=None):
  if(getPlan(user) == None):
    return redirect('/plan/create')

  return redirect('/plan/w/1')

@app.route("/plan/create", methods = ['POST'])
@middlewares.parseUser
@middlewares.requireLogin
def planCreatePOST(user=None, prepedUser=None):
  if(getPlan(user)):
    return redirect('/plan/w/1')

  days = [
    prepDayTime(int(request.form["monday"])),
    prepDayTime(int(request.form["tuesday"])),
    prepDayTime(int(request.form["wednesday"])),
    prepDayTime(int(request.form["thursday"])),
    prepDayTime(int(request.form["friday"])),
    prepDayTime(int(request.form["saturday"])),
    prepDayTime(int(request.form["sunday"]))
  ]

  defaultContent = json.dumps({
    "monday": "Génération en cours ...",
    "tuesday": "Génération en cours ...",
    "wednesday": "Génération en cours ...",
    "thursday": "Génération en cours ...",
    "friday": "Génération en cours ...",
    "saturday": "Génération en cours ...",
    "sunday": "Génération en cours ..."
  })


  dbManager.getInstance().db.execute("INSERT INTO programs (USERID, HEIGHT, WEIGHT, AGE, GOAL, NEXTWEEKID, DAYS) VALUES (?, ?, ?, ?, ?, ?, ?)",
                                      (user.userId, int(request.form["height"]), int(request.form["weight"]), int(request.form["age"]), request.form["goal"], 2, json.dumps(days)))
  dbManager.getInstance().db.execute("INSERT INTO programsweeks (USERID, WEEKID, CONTENT) VALUES (?, ?, ?)",
                                      (user.userId, 1, defaultContent))
  dbManager.getInstance().db.commit()

  def genWeek():
    generateWeek(user, 1)

  Thread(target=genWeek).start()

  return redirect("/plan/w/1")

@app.route("/plan/nextweek")
@middlewares.parseUser
@middlewares.requireLogin
def planNextWeek(user=None, prepedUser=None):
  if(not getPlan(user)):
    return redirect('/plan/w/1')

  defaultContent = json.dumps({
    "monday": "Génération en cours ...",
    "tuesday": "Génération en cours ...",
    "wednesday": "Génération en cours ...",
    "thursday": "Génération en cours ...",
    "friday": "Génération en cours ...",
    "saturday": "Génération en cours ...",
    "sunday": "Génération en cours ..."
  })

  nextWeek = getPlan(user)[5]

  dbManager.getInstance().db.execute("UPDATE programs SET NEXTWEEKID=? WHERE USERID=?", (nextWeek+1, user.userId))
  dbManager.getInstance().db.execute("INSERT INTO programsweeks (USERID, WEEKID, CONTENT) VALUES (?, ?, ?)", (user.userId, nextWeek, defaultContent))
  dbManager.getInstance().db.commit()

  def genWeek():
    generateWeek(user, nextWeek)

  Thread(target=genWeek).start()

  return redirect("/plan/w/" + str(nextWeek))

@app.route("/plan/create")
@middlewares.parseUser
@middlewares.requireLogin
def planCreate(user=None, prepedUser=None):
  if(getPlan(user)):
    return redirect('/plan/w/1')
  return render_template('plan.html', active="plan", user=prepedUser, noprogram=True,
                         weeks=[],
                         weekContent={}
                         )

@app.route("/plan/w/<weekid>")
@middlewares.parseUser
@middlewares.requireLogin
def planWeek(user=None, prepedUser=None, weekid=None):
  if(not getPlan(user)):
    return redirect('/plan/create')

  try:
    weekid = int(weekid)
  except:
    return redirect('/plan')

  planWeeks = getPlanWeeks(user)
  weekContent = None

  weekIds = []
  for week in planWeeks:
    weekIds.append(week[2])
    if(weekid == week[2]):
      if(week[3]):
        weekContent = json.loads(week[3])
      else:
        weekContent = {
          "monday": "",
          "tuesday": "",
          "wednesday": "",
          "thursday": "",
          "friday": "",
          "saturday": "",
          "sunday": ""
        }

  return render_template('plan.html', active="plan", user=prepedUser, noprogram=False,
                         weeks=weekIds,
                         weekContent=weekContent
                         )

# Initialization
if __name__ == '__main__':
  password.setSalt(config.salt)
  app.run(port = config.port)