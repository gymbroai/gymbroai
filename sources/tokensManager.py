import jwt
import datetime

from config import config

def generateAuthToken(user):
  payload = {
    "id": user.userId,
    "avatarUrl": user.avatar.format(),
    "exp": datetime.datetime.now() + datetime.timedelta(seconds = config.tokens.authDuration)
  }

  return jwt.encode(payload, config.tokens.auth, algorithm="HS256")

def checkAuthToken(authToken):
  try:
    jwt.decode(authToken, config.tokens.auth, algorithms=["HS256"])
    return True
  except:
    return False
  
def parseAuthToken(authToken):
  try:
    return jwt.decode(authToken, config.tokens.auth, algorithms=["HS256"])
  except:
    return None