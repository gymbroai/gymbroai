from config import config

from db.manager import dbManager
from password import hash_password
from classes.avatar import Avatar

class User():
  def __init__(
    self,
    userId
  ):
    if not userId: raise ValueError
    self.userId = userId
    self.userName = ""
    self.fullName = ""
    self.email = ""
    self.avatar = None
      
    self.__loadUser()

    pass

  def __loadUser(self):
    # Load user from database
    dbUser = dbManager.getInstance().db.selectUserData(self.userId)
    self.userName = dbUser[1]
    self.fullName = dbUser[2]
    self.email = dbUser[3]
    self.avatar = Avatar.decode(dbUser[5])

    pass

  @staticmethod
  def login(
    username: str,
    password: str
  ):
    password = hash_password(password)
    u = dbManager.getInstance().db.selectOne("SELECT ID FROM users WHERE USERNAME = ? AND PASSWORD = ?", (username, password))

    if(u == None):
      return None

    return User(u[0])

  def formatForWeb(self):
    return {
      "avatar": self.avatar.format()
    }