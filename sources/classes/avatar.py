from enum import Enum
import random
import json

baseUrl = "https://api.dicebear.com/7.x/big-ears-neutral/jpg"

class parameterName():
  def __init__(self, paramName):
    self.paramName = paramName
    pass

  def __call__(self, *args):
    args[0].paramName = self.paramName
    return args[0]

class AvatarParameter(Enum):
  @classmethod
  def random(self):
    return random.choice(list(self))

  def format(self) -> str:
    return f"{self.paramName}={self.value}"

class AvatarBackgroundParameter():
  def __init__(self, color: str):
    self.color = color
    pass

  @staticmethod
  def random():
    choices = ["ffdfbf"]
    return AvatarBackgroundParameter(color = random.choice(choices))

  def format(self) -> str:
    return f"backgroundColor={self.color}"

@parameterName("cheek")
class AvatarCheekParameter(AvatarParameter):
  NONE = "none"
  VARIANT01 = "variant01"
  VARIANT02 = "variant02"
  VARIANT03 = "variant03"
  VARIANT04 = "variant04"
  VARIANT05 = "variant05"
  VARIANT06 = "variant06"

  def format(self) -> str:
    if(self.value == AvatarCheekParameter.NONE.value):
      return "cheekProbability=0"
    return f"{self.paramName}={self.value}"

@parameterName("eyes")
class AvatarEyesParameter(AvatarParameter):
  # VARIANT01 = "variant01"
  VARIANT02 = "variant02"
  VARIANT03 = "variant03"
  VARIANT04 = "variant04"
  VARIANT05 = "variant05"
  VARIANT06 = "variant06"
  VARIANT07 = "variant07"
  VARIANT08 = "variant08"
  VARIANT09 = "variant09"
  VARIANT10 = "variant10"
  VARIANT11 = "variant11"
  VARIANT12 = "variant12"
  VARIANT13 = "variant13"
  VARIANT14 = "variant14"
  VARIANT15 = "variant15"
  VARIANT16 = "variant16"
  VARIANT17 = "variant17"
  VARIANT18 = "variant18"
  VARIANT19 = "variant19"
  VARIANT20 = "variant20"
  VARIANT21 = "variant21"
  VARIANT22 = "variant22"
  VARIANT23 = "variant23"
  VARIANT24 = "variant24"
  # VARIANT25 = "variant25"
  VARIANT26 = "variant26"
  VARIANT27 = "variant27"
  VARIANT28 = "variant28"
  VARIANT29 = "variant29"
  VARIANT30 = "variant30"
  VARIANT31 = "variant31"
  VARIANT32 = "variant32"

@parameterName("mouth")
class AvatarMouthParameter(AvatarParameter):
  VARIANT0101 = "variant0101"
  VARIANT0102 = "variant0102"
  VARIANT0103 = "variant0103"
  VARIANT0104 = "variant0104"
  VARIANT0105 = "variant0105"
  VARIANT0201 = "variant0201"
  VARIANT0202 = "variant0202"
  VARIANT0203 = "variant0203"
  VARIANT0204 = "variant0204"
  VARIANT0205 = "variant0205"
  VARIANT0301 = "variant0301"
  VARIANT0302 = "variant0302"
  VARIANT0303 = "variant0303"
  VARIANT0304 = "variant0304"
  VARIANT0305 = "variant0305"
  VARIANT0401 = "variant0401"
  VARIANT0402 = "variant0402"
  VARIANT0403 = "variant0403"
  VARIANT0404 = "variant0404"
  VARIANT0405 = "variant0405"
  VARIANT0501 = "variant0501"
  VARIANT0502 = "variant0502"
  VARIANT0503 = "variant0503"
  VARIANT0504 = "variant0504"
  VARIANT0505 = "variant0505"
  VARIANT0601 = "variant0601"
  VARIANT0602 = "variant0602"
  VARIANT0603 = "variant0603"
  VARIANT0604 = "variant0604"
  VARIANT0605 = "variant0605"
  VARIANT0701 = "variant0701"
  VARIANT0702 = "variant0702"
  VARIANT0703 = "variant0703"
  VARIANT0704 = "variant0704"
  VARIANT0705 = "variant0705"
  VARIANT0706 = "variant0706"
  VARIANT0707 = "variant0707"
  VARIANT0708 = "variant0708"

@parameterName("nose")
class AvatarNoseParameter(AvatarParameter):
  VARIANT01 = "variant01"
  VARIANT02 = "variant02"
  VARIANT03 = "variant03"
  VARIANT04 = "variant04"
  VARIANT05 = "variant05"
  VARIANT06 = "variant06"
  VARIANT07 = "variant07"
  VARIANT08 = "variant08"
  VARIANT09 = "variant09"
  VARIANT10 = "variant10"
  VARIANT11 = "variant11"
  VARIANT12 = "variant12"

class Avatar:
  def __init__(
    self,
    background: AvatarBackgroundParameter,
    cheek: AvatarCheekParameter,
    eyes: AvatarEyesParameter,
    mouth: AvatarMouthParameter,
    nose: AvatarNoseParameter
  ):
    self.parameters = {}
    self.parameters["background"] = background
    self.parameters["cheek"] = cheek
    self.parameters["eyes"] = eyes
    self.parameters["mouth"] = mouth
    self.parameters["nose"] = nose
    pass
  
  @staticmethod
  def random():
    return Avatar(
      background = AvatarBackgroundParameter.random(),
      cheek = AvatarCheekParameter.NONE,
      eyes = AvatarEyesParameter.random(),
      mouth = AvatarMouthParameter.random(),
      nose = AvatarNoseParameter.random()
    )
  
  @staticmethod
  def decode(txt: str):
    jsonDecoded = json.loads(txt)
    return Avatar(
      background = AvatarBackgroundParameter(color = jsonDecoded['backgroundcolor']),
      cheek = AvatarCheekParameter[jsonDecoded['cheek']],
      eyes = AvatarEyesParameter[jsonDecoded['eyes']],
      mouth = AvatarMouthParameter[jsonDecoded['mouth']],
      nose = AvatarNoseParameter[jsonDecoded['nose']]
    )

  def encode(self):
    return json.dumps({
      "backgroundcolor": self.parameters["background"].color,
      "cheek": self.parameters["cheek"].name,
      "eyes": self.parameters["eyes"].name,
      "mouth": self.parameters["mouth"].name,
      "nose": self.parameters["nose"].name,
    })
  
  def format(self):
    parametersTxt = "?"

    for parameter in self.parameters.values():
      parametersTxt += parameter.format() + "&"

    return baseUrl + parametersTxt