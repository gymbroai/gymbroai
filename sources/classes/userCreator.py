from enum import Enum
from db.manager import dbManager

from password import hash_password
from classes.avatar import Avatar

class UserCreatorSaveError(Enum):
  noUserName = -1
  noFullName = -2
  noEmail = -3
  noPassword = -4
  noAvatar = -5
  userNameTaken = -10
  emailTaken = -11
  noDB = -100

class UserCreator:
  def __init__(
    self,
    userName: str = None,
    fullName: str = None,
    email: str = None,
    password: str = None,
  ):
    self.userName = userName
    self.fullName = fullName
    self.email = email
    self.password = hash_password(password)
    self.avatar = Avatar.random()
    pass

  def setUserName(self, userName):
    self.userName = userName
    return self
  
  def setFullName(self, fullName):
    self.fullName = fullName
    return self
  
  def setEmail(self, email):
    self.email = email
    return self
  
  def setPassword(self, password):
    self.password = hash_password(password)
    return self
  
  def setAvatar(self, avatar):
    self.avatar = avatar
    return self
  
  def save(self):
    if not dbManager.getInstance().db.isConnected(): return UserCreatorSaveError.noDB
    if not self.userName: return UserCreatorSaveError.noUserName
    if not self.fullName: return UserCreatorSaveError.noFullName
    if not self.email: return UserCreatorSaveError.noEmail
    if not self.password: return UserCreatorSaveError.noPassword
    if not self.avatar: return UserCreatorSaveError.noAvatar

    if dbManager.getInstance().db.selectOne("SELECT * FROM users WHERE USERNAME = ?", (self.userName,)):
      return UserCreatorSaveError.userNameTaken

    if dbManager.getInstance().db.selectOne("SELECT * FROM users WHERE EMAIL = ?", (self.email,)):
      return UserCreatorSaveError.emailTaken

    id = dbManager.getInstance().db.insertUser(self.userName, self.fullName, self.email, self.password, self.avatar.encode())

    return id