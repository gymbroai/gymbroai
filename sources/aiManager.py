from config import config
from openai import OpenAI
import re
import json

def formatDaysPrompt(daysList):
  if(len(daysList) != 7): return ""
  daysTxt = "Sessions days and time:"
  if((time := daysList[0]) > 0): daysTxt += f"\n  - Monday: {time}min"
  if((time := daysList[1]) > 0): daysTxt += f"\n  - Tuesday: {time}min"
  if((time := daysList[2]) > 0): daysTxt += f"\n  - Wednesday: {time}min"
  if((time := daysList[3]) > 0): daysTxt += f"\n  - Thursday: {time}min"
  if((time := daysList[4]) > 0): daysTxt += f"\n  - Friday: {time}min"
  if((time := daysList[5]) > 0): daysTxt += f"\n  - Saturday: {time}min"
  if((time := daysList[6]) > 0): daysTxt += f"\n  - Sundauy: {time}min"
  return daysTxt

basePrompt = """
You are an assistant creating weekly gym planings for users that MUST follow the instructions provided next:
  - You MUST follow time constrains and days of the user down to the minute and include warmup and stretching to it. (Time constrains are given in the "Sessions days and time" part of the prompt)
  - YOU MUST FOLLOW TIME CONSTRAINS!
  - Make sure that the individual does not train the upper body and the lower body in the same session!
  - You format like in the given example below! Don't include rest days in the response!
  - Split the exercises into parts like in the example (The parts splitting must make sense)!
  - Don't use 3 sets of 12 reps every time, YOU MUST add some real variation.
  - Stretching and warmup should not take more than 10 minutes each.
  - Stretching exercises don't last for 5 minutes each, try to fit 4 or 5 exercises for the stretching.
  - Warmup and Stretching exercises are not divided in sets but rather they are divied in time.
  - Don't add a little note at the end!

Here is an example:
Monday (x minutes):
- Warmup (10 minutes):
  - Warmup Exercise 1 (x minutes)
  - Warmup Exercise 2 (x minutes)
  ...

- Part 1 (Part NAME) (x minutes):
  - Exercise 1 (x sets of x reps) (x minutes)
  - Exercise 2 (x sets of x reps) (x minutes)
  ...

- Part 2 (Part NAME) (x minutes):
  - Exercise 1 (x sets of x reps) (x minutes)
  - Exercise 2 (x sets of x reps) (x minutes)
  ...

... parts 3 and more if they are some
  
- Stretching (10 minutes):
  - Stretching Exercise 1 (x minutes)
  - Stretching Exercise 2 (x minutes)
  ...
"""

aiClient = OpenAI(api_key = config.openai.secret)

def parseWorkout(input_text):
  workout_routine = {
    "monday": "",
    "tuesday": "",
    "wednesday": "",
    "thursday": "",
    "friday": "",
    "saturday": "",
    "sunday": ""
  }

  day_pattern = re.compile(r"^(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)", re.IGNORECASE)

  current_day = None

  for line in input_text.split("\n"):
    day_match = day_pattern.match(line)
    
    if day_match:
      current_day = day_match.group(1).lower()

    if not day_match and current_day:
      workout_routine[current_day] += (line + "\n")	 

  return workout_routine

def generateWorkout(height, weight, age, goal, weekid, daysList):
  try:
    data = f"""
    Height: {height}cm
    Weight: {weight}kg
    Age: {age}

    Goal: {goal}
    Workout Week Number: {weekid}

    {formatDaysPrompt(daysList)}
    """

    chat_completion = aiClient.chat.completions.create(
      model="gpt-3.5-turbo",
      messages=[
        {"role": "system", "content": basePrompt},
        {"role": "user", "content": data}
      ]
    )

    completionText = chat_completion.choices[0].message.content

    return completionText
  except:
    return None