import db.local

class dbManager():
  instance = None

  @staticmethod
  def getInstance():
    if(dbManager.instance == None):
      dbManager.instance = dbManager()
    return dbManager.instance

  def __init__(
    self,
  ):
    self.db = db.local.DBLocal()