from abc import ABC, abstractmethod

class DBInterface(ABC):
  @abstractmethod
  def isConnected(self) -> bool: pass

  @abstractmethod
  def getCursor(self): pass
  
  @abstractmethod
  def commit(self): pass

  @abstractmethod
  def execute(self, query, values = None): pass

  @abstractmethod
  def selectOne(self, query, values = None): pass

  @abstractmethod
  def selectAll(self, query, values = None): pass

  @abstractmethod
  def insertUser(self, username, fullname, email, password, avatar) -> int: pass

  @abstractmethod
  def selectUserData(self, userId): pass
