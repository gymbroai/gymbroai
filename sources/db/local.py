from db.dbInterface import DBInterface

import sqlite3

class DBLocal(DBInterface):
  def __init__(self):
    super().__init__()
    self.db = sqlite3.connect("data.db", check_same_thread=False)
    self.execute("CREATE TABLE IF NOT EXISTS users"
      + "(ID INTEGER PRIMARY KEY,"
      
      + "USERNAME VARCHAR(100) NOT NULL,"
      + "FULLNAME VARCHAR(100) NOT NULL,"
      + "EMAIL VARCHAR(100) NOT NULL,"
			+ "PASSWORD VARCHAR(64) NOT NULL,"
      
			+ "AVATAR MEDIUMTEXT"

      + ")")
    self.execute("CREATE TABLE IF NOT EXISTS programs"
      + "(ID INTEGER PRIMARY KEY,"
      
      + "USERID INT NOT NULL,"
      + "HEIGHT INT NOT NULL,"
      + "WEIGHT INT NOT NULL,"
      + "AGE INT NOT NULL,"

      + "NEXTWEEKID INT NOT NULL DEFAULT 1,"

      + "GOAL VARCHAR(200) NOT NULL,"
      + "DAYS VARCHAR(200) NOT NULL"

      + ")")
    self.execute("CREATE TABLE IF NOT EXISTS programsweeks"
      + "(ID INTEGER PRIMARY KEY,"
      
      + "USERID INT NOT NULL,"
      + "WEEKID INT NOT NULL,"

      + "CONTENT LONGTEXT"

      + ")")
    
  def isConnected(self) -> bool: return True
  def getCursor(self): return self.db.cursor()
  def commit(self): return self.db.commit()

  def execute(self, query, values = None):
    cursor = self.getCursor()
    if values is None: cursor.execute(query)
    else: cursor.execute(query, values)
    return cursor
  
  def selectOne(self, query, values=None):
    return self.getCursor().execute(query, values).fetchone()

  def selectAll(self, query, values=None):
    return self.getCursor().execute(query, values).fetchall()

  def insertUser(self, username, fullname, email, password, avatar) -> int:
    c = self.execute(
      "INSERT INTO users (USERNAME, FULLNAME, EMAIL, PASSWORD, AVATAR) VALUES (?, ?, ?, ?, ?)",
      (username, fullname, email, password, avatar)
    )
    self.commit()
    return c.lastrowid
  
  def selectUserData(self, userId):
    return self.execute("SELECT * FROM users WHERE ID = ?", (userId,)).fetchone()