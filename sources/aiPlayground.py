from config import config
from openai import OpenAI
import re
import json

import pyinputplus as pyip
from pick import pick

class bcolors:
  HEADER = '\033[95m'
  OKBLUE = '\033[94m'
  OKCYAN = '\033[96m'
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  BOLD = '\033[1m'
  UNDERLINE = '\033[4m'

goalsNames = [
  f"Prise de masse musculaire",
  f"Perte de masse graisseuse",
  f"Renforcement musculaire (Karting)",
  f"Renforcement musculaire (Foot)",
]

goals = [
  "Bulk Up",
  "Cut (Cardio is important)",
  "Go-Kart muscular reinforcement",
  "Soccer muscular reinforcement",
]

def daysPrompt():
  days = []
  print("")
  print(f'{bcolors.OKCYAN}Configurations des jours:{bcolors.ENDC} (Entrez le temps en minute de la séance pour un jour, {bcolors.FAIL}0 pour aucune séance{bcolors.ENDC})')
  days.append(pyip.inputInt(prompt = f'{bcolors.ENDC}  Lundi: {bcolors.OKCYAN}', min = 0, max = 180))
  days.append(pyip.inputInt(prompt = f'{bcolors.ENDC}  Mardi: {bcolors.OKCYAN}', min = 0, max = 180))
  days.append(pyip.inputInt(prompt = f'{bcolors.ENDC}  Mercredi: {bcolors.OKCYAN}', min = 0, max = 180))
  days.append(pyip.inputInt(prompt = f'{bcolors.ENDC}  Jeudi: {bcolors.OKCYAN}', min = 0, max = 180))
  days.append(pyip.inputInt(prompt = f'{bcolors.ENDC}  Vendredi: {bcolors.OKCYAN}', min = 0, max = 180))
  days.append(pyip.inputInt(prompt = f'{bcolors.ENDC}  Samedi: {bcolors.OKCYAN}', min = 0, max = 180))
  days.append(pyip.inputInt(prompt = f'{bcolors.ENDC}  Dimanche: {bcolors.OKCYAN}', min = 0, max = 180))
  print(f'{bcolors.ENDC}')
  return days

def formatDaysPrompt(daysList):
  if(len(daysList) != 7): return ""
  daysTxt = "Sessions days and time:"
  if((time := daysList[0]) > 0): daysTxt += f"\n  - Monday: {time}min"
  if((time := daysList[1]) > 0): daysTxt += f"\n  - Tuesday: {time}min"
  if((time := daysList[2]) > 0): daysTxt += f"\n  - Wednesday: {time}min"
  if((time := daysList[3]) > 0): daysTxt += f"\n  - Thursday: {time}min"
  if((time := daysList[4]) > 0): daysTxt += f"\n  - Friday: {time}min"
  if((time := daysList[5]) > 0): daysTxt += f"\n  - Saturday: {time}min"
  if((time := daysList[6]) > 0): daysTxt += f"\n  - Sundauy: {time}min"
  return daysTxt

# Code

aiClient = OpenAI(api_key = config.openai.secret)

basePrompt = """
You are an assistant creating weekly gym planings for users that MUST follow the instructions provided next:
  - You MUST follow time constrains and days of the user down to the minute and include warmup and stretching to it. (Time constrains are given in the "Sessions days and time" part of the prompt)
  - YOU MUST FOLLOW TIME CONSTRAINS!
  - Make sure that the individual does not train the upper body and the lower body in the same session!
  - You format like in the given example below! Don't include rest days in the response!
  - Split the exercises into parts like in the example (The parts splitting must make sense)!
  - Don't use 3 sets of 12 reps every time, YOU MUST add some real variation.
  - Stretching and warmup should not take more than 10 minutes each.
  - Stretching exercises don't last for 5 minutes each, try to fit 4 or 5 exercises for the stretching.
  - Warmup and Stretching exercises are not divided in sets but rather they are divied in time.
  - Don't add a little note at the end!

Here is an example:
Monday (x minutes):
- Warmup (10 minutes):
  - Warmup Exercise 1 (x minutes)
  - Warmup Exercise 2 (x minutes)
  ...

- Part 1 (Part NAME) (x minutes):
  - Exercise 1 (x sets of x reps) (x minutes)
  - Exercise 2 (x sets of x reps) (x minutes)
  ...

- Part 2 (Part NAME) (x minutes):
  - Exercise 1 (x sets of x reps) (x minutes)
  - Exercise 2 (x sets of x reps) (x minutes)
  ...

... parts 3 and more if they are some
  
- Stretching (10 minutes):
  - Stretching Exercise 1 (x minutes)
  - Stretching Exercise 2 (x minutes)
  ...
"""

print("")
print(f"====[{bcolors.OKGREEN}GymBRO AI PLAYGROUND{bcolors.ENDC}]====")
height = pyip.inputInt(prompt = f'{bcolors.ENDC}Taille (cm): {bcolors.OKCYAN}', min = 100, max = 230)
weight = pyip.inputInt(prompt = f'{bcolors.ENDC}Poids (kg): {bcolors.OKCYAN}', min = 30, max = 200)
age = pyip.inputInt(prompt = f'{bcolors.ENDC}Age: {bcolors.OKCYAN}', min = 10, max = 100)
weekid = pyip.inputInt(prompt = f'{bcolors.ENDC}Weekid: {bcolors.OKCYAN}')
print(bcolors.ENDC)

goalName, goalId = pick(goalsNames, "Objectif: ", indicator='=>', default_index=0)
goal = goals[goalId]

print("Objectif: " + goalName)

daysList = daysPrompt()

data = f"""
Height: {height}cm
Weight: {weight}kg
Age: {age}

Goal: {goal}
Workout Week Number: {weekid}

{formatDaysPrompt(daysList)}
"""

print(f"====[{bcolors.OKGREEN}Generating, Please wait{bcolors.ENDC}]====")

chat_completion = aiClient.chat.completions.create(
  model="gpt-3.5-turbo",
  messages=[
    {"role": "system", "content": basePrompt},
    {"role": "user", "content": data}
  ]
)

completionText = chat_completion.choices[0].message.content

def parse_workout_routine(input_text):
  workout_routine = {
    "monday": [],
    "tuesday": [],
    "wednesday": [],
    "thursday": [],
    "friday": [],
    "saturday": [],
    "sunday": []
  }

  day_pattern = re.compile(r"^(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)", re.IGNORECASE)
  session_pattern = re.compile(r"^- (.+?)(?: \((\d+) minutes\))?:$")
  exercise_pattern = re.compile(r"^  - (.+?) \((\d+) minute(s?)\)|^- (.+?) \((\d+) sets of (\d+) reps\)")
  
  current_day = None
  current_session = None

  for line in input_text.split("\n"):
    day_match = day_pattern.match(line)
    session_match = session_pattern.match(line)
    exercise_match = exercise_pattern.match(line)
    
    if day_match:
      current_day = day_match.group(1).lower()
      continue
    
    if session_match:
      session_name, session_duration = session_match.groups()
      durration = int(session_duration) if session_duration else "Unknown"
      current_session = {"name": session_name, "duration": durration, "exercises": []}
      workout_routine[current_day].append(current_session)
      continue
    
    if exercise_match:
      exercise_name, exercise_duration, _, exercise_name_sets, sets, reps = exercise_match.groups()
      if exercise_name:
        current_session["exercises"].append({"name": exercise_name, "duration": int(exercise_duration)})
      else:
        current_session["exercises"].append({
          "name": exercise_name_sets,
          "duration": int(sets) * int(reps),  # Simplified duration calculation for sets/reps
          "sets": f"{sets} sets of {reps} reps"
        })

  return workout_routine

print(completionText)

print("")
print("")
print("")

print(json.dumps(parse_workout_routine(completionText), indent=2))