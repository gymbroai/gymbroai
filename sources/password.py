import hashlib

salt = ""

def setSalt(newSalt):
  global salt
  salt = newSalt

def hash_password(password):
  global salt
  if type(password) != str:	return None
  password_hash = hashlib.sha256((password + salt).encode('utf-8')).hexdigest()
  return password_hash

def verify_password(stored_password, provided_password):
  return hash_password(provided_password) == stored_password