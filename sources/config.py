import json

class openaiConfig():
  def __init__(
    self,
    secret: str
  ) -> None:
    self.secret = secret

class dbConfig():
  def __init__(
    self,
    db
  ) -> None:
    self.host = db["host"]
    self.port = db["port"]
    self.user = db["user"]
    self.password = db["password"]
    self.database = db["database"]

class tokensConfig():
  def __init__(
    self,
    tokens
  ) -> None:
    self.refresh = tokens["refresh"]
    self.refreshDuration = tokens["refreshDurationDays"] * 24 * 60 * 60
    self.auth = tokens["auth"]
    self.authDuration = tokens["authDurationMinutes"] * 60

class configClass():
  def __init__(
    self,
    openaiSecret: str,
    db,
    salt: str,
    port: int,
    tokens
  ) -> None:
    self.openai = openaiConfig(secret = openaiSecret)
    self.db = dbConfig(db)
    self.salt = salt
    self.port = port
    self.tokens = tokensConfig(tokens)

with open('config.json', 'r') as f:
  parsedConfig = json.loads(f.read())
  
  config = configClass(
    openaiSecret = parsedConfig["openai"]["secret"],
    db = parsedConfig["db"],
    salt = parsedConfig["salt"],
    port = parsedConfig["port"],
    tokens = parsedConfig["tokens"],
  )